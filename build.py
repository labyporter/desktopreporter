import os
import shutil
os.chdir(os.path.dirname(os.path.abspath(__file__)))
for directory, dirnames, filenames in os.walk(os.getcwd()):
        if directory.endswith('__pycache__'):
            shutil.rmtree(directory)
os.system('pyinstaller --noconfirm --onefile --windowed --clean --add-data "./src/fonts;fonts/" --add-data "./src/pdfs;pdfs/" --add-data "./src/templates;templates/"  "./src/launcher.py"')
shutil.rmtree('./build')
os.remove('launcher.spec')
