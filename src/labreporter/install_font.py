import os
import subprocess
def install_fonts(i):
    _TEMPL  = """ 
    Set objShell = CreateObject("Shell.Application")
    Set objFolder = objShell.Namespace("%s")
    Set objFolderItem = objFolder.ParseName("%s")
    objFolderItem.InvokeVerb("Install")
    """
    fontpath = os.path.join(os.getcwd(),'fonts/')
    vbspath = os.path.join(fontpath,'fontinst.vbs')
    print(fontpath)
    for directory, dirnames, filenames in os.walk(fontpath):
        for filename in filenames:
            fpath = os.path.join(directory, filename)
            if fpath[-4:] == ".ttf" and filename in i: # modify this line for including multiple extension
                with open(vbspath, 'w') as _f:
                    _f.write(_TEMPL%(directory, filename))
                subprocess.call(['cscript.exe', vbspath])
    os.remove(vbspath)