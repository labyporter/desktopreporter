LABS = {
    "labs":{
        
        "abbott":{
            
            "display":"Antigen - Abbott Labs",
            "file_name":"abbott_antigen_covid.pdf",
            "fonts":['DejaVuSans-Bold.ttf','DejaVuSans.ttf'],
            "fields":[
                {
                    "type":"textRequired",
                    "field_name":"fName",
                    "display_name":"First Name"
                },
                {
                    "type":"textOptional",
                    "field_name":"mName",
                    "display_name":"Middle Name"
                },
                {
                    "type":"textRequired",
                    "field_name":"lName",
                    "display_name":"Last Name"
                },
                {
                    "type":"date",
                    "field_name":"dob",
                    "display_name":"Date Of Birth"
                },
                {
                    "type":"radio",
                    "field_name":"gender",
                    "display_name":"Gender",
                    "display_value_dict":[["Male","Male"],["Female","Female"]]
                },
                {
                    "type":"datetime",
                    "field_name":"tof",
                    "display_name":"Date & Time Of Flight"
                },
                {
                    "type":"textHidden",
                    "field_name":"lab",
                    "field_value":"abbott"
                },
                {
                    "type":"num",
                    "field_name":"tnb4f",
                    "display_name":"Hours Needed Before Flight"
                },
                {
                    "type":"radio",
                    "field_name":"time_adjust",
                    "display_name":"Should we adjust test time not to be on Fri 5PM - Sat 9PM\n(This may still occur if unavoidable)",
                    "display_value_dict":[["true","Yes"],["false","No"]]
                },
            ]
        }
    }
}