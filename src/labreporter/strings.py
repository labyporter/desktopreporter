#Strings
OPENING_PAGE_HEADER = 'Lab Reporter'
OPENING_PAGE_SUBHEADER = 'Welcome, press next to continue'
SELECT_LAB_PAGE_HEADER = 'Lab Selection'
SELECT_LAB_PAGE_SUBHEADER = ''
PERSONAL_INFO_PAGE_HEADER = 'Personal Info'
PERSONAL_INFO_PAGE_SUBHEADER = ''
CREATE_PDF_PAGE_HEADER = 'Created !'
CREATE_PDF_PAGE_SUBHEADER = """Quadruple check to make sure
that the test date is one
that works with your schedule

Press Done to exit."""