from random import randint
from datetime import datetime, timedelta
import tkinter as tk
from tkinter import filedialog
import uuid
from .fill_pdf import write_fillable_pdf
from .labs import LABS
from .install_font import install_fonts

import os

def testTime(a,tnb4f,time_adjust=True):
    og = datetime.strptime(a,r'%Y-%m-%dT%H:%M')
    dt = og - timedelta(hours=int(tnb4f)-1,seconds=randint(1000,3500))
    while((dt.weekday() == 5 and dt.hour < 21) or (dt.weekday() == 4 and dt.hour > 17)) if time_adjust else False or (dt.hour > 18 or dt.hour < 7):
        dt = dt + timedelta(seconds=randint(1000,3660))
    x = dt - og
    if x.total_seconds() > 0:
        dt = og - timedelta(hours=int(tnb4f)-1,seconds=randint(1000,3500))
    return dt

def makePDF(a):
    x ={}
    if a['lab'] == 'abbott':
        install_fonts(LABS['labs'][a['lab']]['fonts'])
        x['full_name'] = f"{a['fName']} {a['mName']} {a['lName']}" if a['mName'] != '' else f"{a['fName']} {a['lName']}"
        x['dob_mm/dd/yyyy'] = f"{a['dob'][-5:-3]}/{a['dob'][-2:]}/{a['dob'][:4]}"
        x['gender'] = a['gender']
        tt = testTime(a['tof'],a['tnb4f'],time_adjust=a['time_adjust'])
        x['dot'] = tt.strftime(r'%m/%d/%Y')
        x['ses_id'] = str(uuid.uuid4())
        x['start_time'] = tt.strftime(r'%m/%d/%Y %I:%M:%S %p UTC')
        x['end_time'] = (tt + timedelta(seconds=randint(1000,3500))).strftime(r'%m/%d/%Y %I:%M:%S %p UTC')
        root = tk.Tk()
        root.withdraw()
        path_to_pref = filedialog.asksaveasfilename(
            defaultextension='.pdf',
            filetypes=[("Portable Document Files (*.pdf)", '*.pdf')],
            title="Input Filename")
        root.focus()
        if path_to_pref != None:
            write_fillable_pdf(f"./pdfs/{LABS['labs'][a['lab']]['file_name']}", path_to_pref, x)