import os
from flask import Flask
from flask import request

from .server_functions import *

#Define Flask Server
print(os.path.join(os.getcwd(),'templates'))
app = Flask(__name__,template_folder=os.path.join(os.getcwd(),'templates'))

#Set Opening Page
@app.route('/')
def opening_page():
    return opening_page_func(request)

@app.route('/selectLab')
def select_lab_page():
    return select_lab_page_func(request)

@app.route('/personalInfo')
def personal_info__page():
    return personal_info_page_func(request)

@app.route('/createPdf')
def create_pdf__page():
    return create_pdf_page_func(request)

#Set Run Function
def start_server():
    app.run(host='0.0.0.0', port=80)

#Set Api
class Api:
    def __init__(self):
        self._window = None
    def set_window(self, window):
        self._window = window
    def destroy(self):
        self._window.destroy()