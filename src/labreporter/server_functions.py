from flask import render_template

from .strings import *
from .labs import LABS
from .make_pdf import makePDF

def opening_page_func(request):
    h = OPENING_PAGE_HEADER
    sh = OPENING_PAGE_SUBHEADER
    f = []
    u = '/selectLab'
    return render_template('index.html',header = h,subheader = sh,fields = f,url = u)

def select_lab_page_func(request):
    h = SELECT_LAB_PAGE_HEADER
    sh = SELECT_LAB_PAGE_SUBHEADER
    f = [{
        "type":"radio",
        "field_name":"lab",
        "display_name":"Select Your Lab",
        "display_value_dict":[[i,o['display']] for i , o in LABS['labs'].items()]
    }]
    u = '/personalInfo'
    return render_template('index.html',header = h,subheader = sh,fields = f,url = u)

def personal_info_page_func(request):
    h = SELECT_LAB_PAGE_HEADER
    sh = SELECT_LAB_PAGE_SUBHEADER
    f = LABS['labs'][request.args['lab']]['fields']
    u = '/createPdf'
    return render_template('index.html',header = h,subheader = sh,fields = f,url = u, test = request.args['lab'])


def create_pdf_page_func(request):
    makePDF(dict(request.args))
    h = CREATE_PDF_PAGE_HEADER
    sh = CREATE_PDF_PAGE_SUBHEADER
    f = LABS['labs'][request.args['lab']]['fields']
    u = False
    return render_template('index.html',header = h,subheader = sh,fields = f,url = u, test = request.args['lab'])