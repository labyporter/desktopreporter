# Imports
import sys
import os
os.chdir(sys._MEIPASS if getattr(sys, 'frozen', False) else os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import threading
from .server import *
import webview

def run():
    t = threading.Thread(target=start_server)
    t.daemon = True
    t.start()
    api = Api()
    window = webview.create_window("Test Result Generator", "http://localhost/",width=400, height=600,resizable=False,js_api=api)
    api.set_window(window)
    webview.start()
    sys.exit()

if __name__ == '__main__':
    run()